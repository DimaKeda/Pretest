﻿using Dapper;
using latihan_netcore.Models;

namespace latihan_netcore.Repository
{
    public interface IJWTAuthManager
    {
        Response<string> GenerateJWT(ModelUser user);
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getUserList<T>();
        Response<List<T>> getCompanyList<T>();
        /*Response<List<T>> getProductList<T>();*/
        Response<List<T>> getPositiontList<T>();
        Response<List<T>> getDocumentCategorytList<T>();
        Response<List<T>> getDocumentList<T>();
        /*object GenerateJWT(ModelCompany data);*/
    }
}
