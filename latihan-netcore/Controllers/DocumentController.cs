﻿using Dapper;
using latihan_netcore.Models;
using latihan_netcore.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;


namespace latihan_netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public DocumentController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        /*[Microsoft.AspNetCore.Mvc.HttpPost("Login")]*/
        [HttpPost("Login")]
        [AllowAnonymous]
        public IActionResult Login([System.Web.Http.FromBody] LoginModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Paramter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("email", user.Email, DbType.String);
            dp_param.Add("password", user.Password, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelUser>("sp_LoginUser", dp_param);

            if (result.Code == 200)
            {
                var token = _authentication.GenerateJWT(result.Data);

                return Ok(token);
            }

            return NotFound(result.Data);
        }


        [HttpGet("DocumentList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getDocument()
        {
            var result = _authentication.getDocumentList<ModelDocument>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocument user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", user.IDCompany, DbType.String);
            dp_param.Add("iduser", user.CreatedBy, DbType.String);
            dp_param.Add("idcategory", user.IDCategory, DbType.String);
            dp_param.Add("name", user.Name, DbType.String);
            dp_param.Add("description", user.Description, DbType.String);
            dp_param.Add("flag", user.Flag, DbType.String);
            dp_param.Add("createby", user.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_createDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(new {data = user});
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocument user, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("idcompany", user.IDCompany, DbType.String);
            dp_param.Add("iduser", user.CreatedBy, DbType.String);
            dp_param.Add("idcategory", user.IDCategory, DbType.String);
            dp_param.Add("name", user.Name, DbType.String);
            dp_param.Add("description", user.Description, DbType.String);
            dp_param.Add("flag", user.Flag, DbType.String);
            dp_param.Add("createby", user.CreatedBy, DbType.Int32);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_updateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_deleteDocument", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
