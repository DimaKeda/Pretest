﻿using System.ComponentModel.DataAnnotations;

namespace latihan_netcore.Models
{
    public class ModelDocument
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? CreatedBy { get; set; }

        public int? IDCategory { get; set; }

        [Required]
        public string? Name { get; set; }
        
        public string? Description { get; set; }
        
        public int? Flag { get; set; }
        
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
