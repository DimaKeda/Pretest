USE [master]
GO
/****** Object:  Database [Pretest]    Script Date: 04/08/2023 09:19:35 ******/
CREATE DATABASE [Pretest] ON  PRIMARY 
( NAME = N'Pretest', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\Pretest.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Pretest_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\Pretest_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Pretest].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Pretest] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Pretest] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Pretest] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Pretest] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Pretest] SET ARITHABORT OFF 
GO
ALTER DATABASE [Pretest] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Pretest] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Pretest] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Pretest] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Pretest] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Pretest] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Pretest] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Pretest] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Pretest] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Pretest] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Pretest] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Pretest] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Pretest] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Pretest] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Pretest] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Pretest] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Pretest] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Pretest] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Pretest] SET  MULTI_USER 
GO
ALTER DATABASE [Pretest] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Pretest] SET DB_CHAINING OFF 
GO
USE [Pretest]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](50) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Document]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Document](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDCategory] [int] NULL,
	[Name] [varchar](255) NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocumentCategory]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_DocumentCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Position]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDPosition] [int] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[ViewDocument]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewDocument]
AS
SELECT        dbo.[Document].Name AS Expr1, dbo.[Document].Description, dbo.[Document].IDCompany, dbo.[Document].IDCategory, dbo.DocumentCategory.Name AS Expr2, dbo.Company.Name
FROM            dbo.Company INNER JOIN
                         dbo.[Document] ON dbo.Company.ID = dbo.[Document].IDCompany INNER JOIN
                         dbo.DocumentCategory ON dbo.Company.ID = dbo.DocumentCategory.ID INNER JOIN
                         dbo.Position ON dbo.Company.ID = dbo.Position.ID INNER JOIN
                         dbo.[User] ON dbo.Company.ID = dbo.[User].IDCompany AND dbo.Position.ID = dbo.[User].IDPosition
GO
SET IDENTITY_INSERT [dbo].[Company] ON 

INSERT [dbo].[Company] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (1, NULL, N'Berijalan', N'JL Mrican', N'berijalan.co.id', N'911', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[Company] OFF
GO
SET IDENTITY_INSERT [dbo].[Document] ON 

INSERT [dbo].[Document] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (1, N'9d834bc4-ca70-45fd-92da-ffdfd7bc0875', 1, 2, N'Final Fantasy 20', N'ini MANTAB', 0, 1, CAST(N'2023-08-03T12:21:30.087' AS DateTime))
SET IDENTITY_INSERT [dbo].[Document] OFF
GO
SET IDENTITY_INSERT [dbo].[DocumentCategory] ON 

INSERT [dbo].[DocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'd143492d-002f-41f2-923a-d137e0e604d7', N'Sci-Fi', 1, CAST(N'2023-08-03T12:13:17.090' AS DateTime))
INSERT [dbo].[DocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (2, N'224b6f1a-37cc-494b-86e0-0cc60f059d9e', N'Fantasy', 1, CAST(N'2023-08-03T12:14:35.930' AS DateTime))
SET IDENTITY_INSERT [dbo].[DocumentCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Position] ON 

INSERT [dbo].[Position] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'969e2873-adb4-4ee3-954e-10911b422e53', N'Tumbal Proyek', 1, CAST(N'2023-08-03T12:25:54.467' AS DateTime))
INSERT [dbo].[Position] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (2, N'7374c4a1-ed96-41f6-aa2c-102d7e5ea77f', N'Magang', 1, CAST(N'2023-08-03T12:26:04.923' AS DateTime))
SET IDENTITY_INSERT [dbo].[Position] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (6, NULL, NULL, NULL, N'Keda', N'JL Kendeng', N'dimakeda@hotmail', N'082235399127', N'Keda', N'202cb962ac59075b964b07152d234b70', N'Admin', NULL, NULL, NULL)
INSERT [dbo].[User] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (8, N'60e3ac78-60ad-43cf-8140-7b9eb572269b', 1, 1, N'Avril', N'JL Semangka', N'avrildiva@gmail.com', N'083345678132', N'diva', N'202CB962AC59075B964B07152D234B70', N'Admin', 2, 1, CAST(N'2023-08-03T12:38:25.690' AS DateTime))
INSERT [dbo].[User] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (11, N'8bf7a584-48ba-402f-922d-9b402b4d106b', 1, 1, N'Sinaga', N'JLnan', N'naga@gmail.com', N'0812325112', N'dragon', N'202CB962AC59075B964B07152D234B70', N'Admin', 0, 1, CAST(N'2023-08-03T20:43:35.963' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_Company1] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_Company1]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_DocumentCategory] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[DocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_DocumentCategory]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Company]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Position] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[Position] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Position]
GO
/****** Object:  StoredProcedure [dbo].[sp_createCompany]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createCompany]
	-- Add the parameters for the stored procedure here

	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Company]
           ([UID]
           ,[Name]
           ,[Address]
		   ,[Email]
           ,[Telephone]
		   ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
 
     VALUES
           (NEWID()
           ,@name
           ,@address
		   ,@email
		   ,@telephone
		   ,@flag
		   ,@iduser
	       ,GETDATE())
				

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createDocument]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createDocument]
	@iduser int,
	@idcompany int,
	@idcategory int,
	@name varchar(100),
	@description text,
	@flag int,
	@createby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Document]
           ([IDCompany]
		   ,[IDcategory]
           ,[UID]
           ,[Name]
           ,[Description]
           ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (@idcompany,
		   @idcategory,
		   NEWID(),
		   @name,
		   @description,
		   @flag,
		   @iduser,
		   GETDATE())
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createDocumentCategory]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createDocumentCategory]
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[DocumentCategory]
           ([UID]
           ,[Name]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID(),
		   @name,
		   @iduser,
		   GETDATE())
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createPosition]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createPosition]
	@iduser int,
	@name varchar(100),
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Position]
           ([UID]
           ,[Name]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (
		   NEWID(),
		   @name,
		   @iduser,
		   GETDATE())
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createUser]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createUser]
	-- Add the parameters for the stored procedure here

	@idcompany int,
	@idposition int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[User]
           ([UID]
		   ,[IDCompany]
		   ,[IDPosition]
           ,[Name]
           ,[Address]
		   ,[Email]
           ,[Telephone]
		   ,[Username]
		   ,[Password]	
		   ,[Role]
		   ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
 
     VALUES
           (NEWID()
		   ,@idcompany
		   ,@idposition
           ,@name
           ,@address
		   ,@email
		   ,@telephone
		   ,@username
		   ,CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2) 
		   ,@role
		   ,@flag
		   ,@iduser
	       ,GETDATE())
				

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteCompany]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteCompany]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Company WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteDocument]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteDocument]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Document WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteDocumentCategory]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteDocumentCategory]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM DocumentCategory WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deletePosition]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deletePosition]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Position WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteUser]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteUser]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [User] WHERE ID = @id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_loginUser]
@email varchar(50),
@password varchar(50),
@retVal int OUTPUT

AS
BEGIN
SET NOCOUNT ON;
	SELECT
		[ID],
		[Username],
		[Email],
		[Role],
		[CreatedAt]
	FROM [User]
	WHERE
		[Email] = @email and [Password] = CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateCompany]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateCompany]
	-- Add the parameters for the stored procedure here

	@id int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update [dbo].[Company] SET
   
           [Name] = @name
           ,[Address] = @address
		   ,[Email] = @email
           ,[Telephone] =@telephone
		   ,[Flag] = @flag
		   ,[CreatedBy] = @iduser
 
	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateDocument]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateDocument]
	@id int,
	@iduser int,
	@idcompany int,
	@idcategory int,
	@name varchar(100),
	@description text,
	@flag int,
	@createby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Document]
   SET [IDCompany] = @idcompany
      ,[Name] = @name
	  ,[IDCategory]=@idcategory
      ,[description] = @description
      ,[Flag] = @flag
      ,[CreatedBy] = @iduser
	WHERE id=@id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateDocumentCategory]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateDocumentCategory]
	@id int,
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[DocumentCategory]
   SET [Name] = @name
      ,[CreatedBy] = @iduser
	WHERE id=@id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updatePosition]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updatePosition]
	@id int,
	@iduser int,
	@name varchar(100),
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Position]
   SET [Name] = @name
      ,[CreatedBy] = @iduser
	WHERE id=@id
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal =200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateUser]    Script Date: 04/08/2023 09:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateUser]
	-- Add the parameters for the stored procedure here

	@idcompany int,
	@idposition int,
	@name varchar(255),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@iduser int,
	@retVal int OUTPUT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update [dbo].[User] SET
           
		   [IDCompany] = @idcompany
		   ,[IDPosition] = @idposition
           ,[Name] = @name
           ,[Address] = @address
		   ,[Email]	= @email
           ,[Telephone]	= @telephone
		   ,[Username]	= @username
		   ,[Password]	= @password
		   ,[Role] = @role
		   ,[Flag] = @flag

 
    WHERE ID = @iduser
				

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Company"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Document"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "DocumentCategory"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 136
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Position"
            Begin Extent = 
               Top = 6
               Left = 662
               Bottom = 136
               Right = 832
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User"
            Begin Extent = 
               Top = 6
               Left = 870
               Bottom = 136
               Right = 1040
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewDocument'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewDocument'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewDocument'
GO
USE [master]
GO
ALTER DATABASE [Pretest] SET  READ_WRITE 
GO
